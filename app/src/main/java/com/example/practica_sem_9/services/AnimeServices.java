package com.example.practica_sem_9.services;

import com.example.practica_sem_9.entities.Anime;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface AnimeServices {
    @GET("animes")
    Call<List<Anime>> getAnime();


    @PUT("animes/{id}")
    Call<Void> editAnime(@Body Anime anime);
}
