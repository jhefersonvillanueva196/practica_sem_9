package com.example.practica_sem_9.entities;

public class Anime {
    public int id;
    public String nombre;
    public boolean favorito;
    public String imagen;


    public Anime() {
    }

    public Anime(String nombre, boolean favorito, String imagen) {
        this.nombre = nombre;
        this.favorito = favorito;
        this.imagen = imagen;
    }

    public Anime(int id, String nombre, boolean favorito, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.favorito = favorito;
        this.imagen = imagen;
    }
}
