package com.example.practica_sem_9;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.practica_sem_9.adapters.AnimeAdapter;
import com.example.practica_sem_9.entities.Anime;
import com.example.practica_sem_9.services.AnimeServices;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    List<Anime> animes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://62863682f0e8f0bb7c126992.mockapi.io/api/PS9/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AnimeServices services = retrofit.create(AnimeServices.class);

        Call<List<Anime>> call = services.getAnime();

        call.enqueue(new Callback<List<Anime>>() {
            @Override
            public void onResponse(Call<List<Anime>> call, Response<List<Anime>> response) {
                if(!response.isSuccessful()){
                    Log.e("Eror creado","Error de aplicacion");
                }else{
                    Log.i("Exito creado","Buena ejecucion");
                    Log.i("Exito creado",new Gson().toJson(response.body()));

                    animes = response.body();

                    AnimeAdapter adapter = new AnimeAdapter(animes);

                    RecyclerView rv = findViewById(R.id.rvAnimes);
                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    rv.setHasFixedSize(true);
                    rv.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Anime>> call, Throwable t) {
                Log.e("Eror creado","No conexion");
            }
        });
    }
}