package com.example.practica_sem_9.adapters;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.practica_sem_9.R;
import com.example.practica_sem_9.entities.Anime;
import com.example.practica_sem_9.services.AnimeServices;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.AnimeViewHolder>{
    List<Anime> animes;
    public AnimeAdapter(List<Anime> animes) {
        this.animes = animes;
    }

    @NonNull
    @Override
    public AnimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_anime, parent, false);
        return new AnimeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimeViewHolder vh, int position) {

        View itemView = vh.itemView;

        Anime anime = animes.get(position);
        ImageView imagen = itemView.findViewById(R.id.imagen);
        TextView tvContactName = itemView.findViewById(R.id.nombreanime);
        MaterialFavoriteButton favorite = itemView.findViewById(R.id.Fav);
        Picasso.with(itemView.getContext())
                .load(new File(anime.imagen))
                .fit()
                .centerInside()
                .into(imagen);
        tvContactName.setText(anime.nombre);
        if(anime.favorito==true){
            favorite.setFavorite(true);
        }else{
            favorite.setFavorite(false);
        }
        favorite.setOnFavoriteChangeListener(new MaterialFavoriteButton.OnFavoriteChangeListener() {
            @Override
            public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                anime.favorito = favorite;
                Retrofit rf = new Retrofit.Builder()
                        .baseUrl("https://62863682f0e8f0bb7c126992.mockapi.io/api/PS9/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                AnimeServices services = rf.create(AnimeServices.class);
                Call<Void> call = services.editAnime(anime);
                Log.i("Dato", "Se dio el cambio: ");

            }
        });



    }

    @Override
    public int getItemCount() {
        return animes.size();
    }

    class AnimeViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {

        public AnimeViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Anime anime = animes.get(i);
            Log.i("APP_VJ20202", "click en el elemento" + anime.id);
        }
    }
}
